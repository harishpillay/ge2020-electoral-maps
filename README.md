# GE2020 Electoral Maps

This is a public repository of all of the electoral maps that show the various districts for the Singapore General Election 2020. The original source is www.eld.gov.sg but it is not clear that it will be archived. So, this serves as an archive.

All the files are available from: https://drive.google.com/drive/folders/1BT109uATTs7Aumxqqx3eDNTuy32T4rzE?usp=sharing. 